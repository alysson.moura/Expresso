package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.logging.Logger;
import static java.util.logging.Level.*;

public class CryptoException extends RuntimeException {

    private static final Logger logger = Logger.getLogger(CryptoException.class.getName());

    public enum Error {
        //TODO: classificar segundo perspectiva de interesse diferenciado do lado JS
        INCORRECT_PIN,
        OPERATION_CANCELLED,
        PRIMARY_KEY_NOT_FOUND,
        TOKEN_NOT_FOUND,
        UNEXPECTED;
    }

    //==========================================================================
    private CryptoException(String message) {
        super(message);
        logger.log(SEVERE, message);
    }

    private CryptoException(String message, Throwable cause) {
        super(message, cause);
        logger.log(SEVERE, message, cause);
    }

    //==========================================================================
    public static <T> T raise(Error error) {
        throw new CryptoException(error.name());
    }

    public static <T> T raise(String message) {
        throw new CryptoException(message);
    }

    public static <T> T raise(Error error, String message) {
        throw new CryptoException(error.name() + " " + message);
    }

    public static <T> T raise(Throwable cause) {
        throw new CryptoException(Error.UNEXPECTED.name(), cause);
    }

    public static <T> T raise(Error error, Throwable cause) {
        throw new CryptoException(error.name(), cause);
    }

    public static <T> T raise(String message, Throwable cause) {
        throw new CryptoException(message, cause);
    }

    public static <T> T raise(Error error, String message, Throwable cause) {
        throw new CryptoException(error.name() + " " + message, cause);
    }

}
