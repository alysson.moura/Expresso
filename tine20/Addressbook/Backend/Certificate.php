<?php
    /**
    * Tine 2.0
    *
    * @package     Addressbook
    * @subpackage  Backend
    * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
    * @author      Mário César Kolling <mario.kolling@serpro.gov.br>
    * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
    * @copyright   Copyright (c) 2009-2014 Serpro (http://serpro.gov.br)
    */

    /**
    * sql backend class for the addressbook certificate
    *
    * @package     Addressbook
    */
    class Addressbook_Backend_Certificate extends Tinebase_Backend_Sql_Abstract {
        /**
        * Table name without prefix
        *
        * @var string
        */
        protected $_tableName = 'addressbook_certificates';

        /**
        * Model name
        *
        * @var string
        */
        protected $_modelName = 'Addressbook_Model_Certificate';

        /**
        * if modlog is active, we add 'is_deleted = 0' to select object in _getSelect()
        *
        * @var boolean
        */
        protected $_modlogActive = FALSE;

        /**
        * Identifier
        *
        * @var string
        */
        protected $_identifier = 'hash';


        /**
        * returns true if id is a hash value and false if integer
        *
        * @return  boolean
        * @todo    remove that when all tables use hash ids
        */
        protected function _hasHashId()
        {
            return true;
        }


    /**
    * converts raw data from adapter into a set of records
    *
    * @param  array $_rawDatas of arrays
    * @return Tinebase_Record_RecordSet
    */
        protected function _rawDataToRecordSet(array $_rawDatas)
        {
            $certs = array();

            foreach($_rawDatas as $cert){
                $objCertificate = Custom_Auth_ModSsl_Certificate_Factory::buildCertificate($cert['certificate'], TRUE);
                if($objCertificate && $objCertificate->isValid()){
                    $certs[] = $cert;
                }
            };
            return parent::_rawDataToRecordSet($certs);
        }


        /**
        * the constructor
        *
        * allowed options:
        *  - modelName
        *  - tableName
        *  - tablePrefix
        *  - modlogActive
        *  - useSubselectForCount
        *
        * @param Zend_Db_Adapter_Abstract $_db (optional)
        * @param array $_options (optional)
        * @throws Tinebase_Exception_Backend_Database
        */
        public function __construct($_dbAdapter = NULL, $_options = array())
        {
            parent::__construct($_dbAdapter, $_options);

    //        $this->_additionalColumns['emails'] = new Zend_Db_Expr('(' .
    //            $this->_db->select()
    //                ->from($this->_tablePrefix . 'addressbook', array($this->_dbCommand->getAggregate('email')))
    //                ->where($this->_db->quoteIdentifier('id') . ' IN ?', $this->_db->select()
    //                    ->from(array('addressbook_list_members' => $this->_tablePrefix . 'addressbook_list_members'), array('contact_id'))
    //                    ->where($this->_db->quoteIdentifier('addressbook_list_members.list_id') . ' = ' . $this->_db->quoteIdentifier('addressbook_lists.id'))
    //            ) .
    //        ')');
        }
    }

?>
