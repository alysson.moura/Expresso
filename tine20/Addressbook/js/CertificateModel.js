/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mário César Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2009-2014 Serpro (http://serpro.gov.br)
 * 
 */

Tine.Addressbook.Model.Certificate = Tine.Tinebase.data.Record.create([
   {name: 'hash'},
   {name: 'auth_key_identifier'},
   {name: 'email'},
   {name: 'certificate'},
   {name: 'invalid'}
], {
    appName: 'Addressbook',
    modelName: 'Certificate',
    idProperty: 'hash',
    recordName: 'Certificate',
    recordsName: 'Certificates'
});

Tine.Addressbook.Model.Certificate.getFilterModel = function() {
    var app = Tine.Tinebase.appMgr.get('Addressbook');
    
    var typeStore = [['certificate', app.i18n._('Certificate')]];
    
    return [
        {label: _('Hash'), field: 'hash', operators: ['equals']},
        {label: _('Authority Key Identifier'), field: 'auth_key_identifier', operators: ['equals']},
        {label: _('Email'), field: 'email', operators: ['in']}
    ];
};

/**
 * default certificate backend
 */
Tine.Addressbook.certificateBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Addressbook',
    modelName: 'Certificate',
    recordClass: Tine.Addressbook.Model.Certificate
});