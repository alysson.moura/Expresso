<?php
/**
 * Tine 2.0
 *
 * @package     Addressbook
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>
 * @copyright   Copyright (c) 2010-2014 SERPRO (http://www.serpro.gov.br)
 */

/**
 * class to hold addressbook Contact list data
 *
 * @property    list_id
 * @property    contact_id
 * @property    email
 * @property    list_id
 * @package     n_fn
 */
class Addressbook_Model_ContactList extends Tinebase_Record_Abstract
{
    /**
     * the emailattribute of the model
     */
    const EMAILATTRIBUTE = 'email';

    /**
     * the name attribute of the model
     *
     */
    const NAMEATTRIBUTE = 'n_fn';

    /**
     * key in $_validators/$_properties array for the filed which
     * represents the identifier
     *
     * @var string
     */
    protected $_identifier = 'list_id';

    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Addressbook';

    /**
     * list of zend validator
     *
     * this validators get used when validating user generated content with Zend_Input_Filter
     *
     * @var array
     */
    protected $_validators = array (
        // tine 2.0 generic fields
        'list_id'              => array(Zend_Filter_Input::ALLOW_EMPTY => false, Zend_Filter_Input::DEFAULT_VALUE => NULL),
        'contact_id'           => array(Zend_Filter_Input::ALLOW_EMPTY => false, Zend_Filter_Input::DEFAULT_VALUE => NULL),
        'email'                => array(Zend_Filter_Input::ALLOW_EMPTY => false, Zend_Filter_Input::DEFAULT_VALUE => 'email'),
        'n_fn'                 => array(Zend_Filter_Input::ALLOW_EMPTY => false, Zend_Filter_Input::DEFAULT_VALUE => 'nome'),
    );
}
