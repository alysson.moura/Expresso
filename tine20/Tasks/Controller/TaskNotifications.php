<?php
/**
 * Tasks Task Notifications
 *
 * @package     Tasks
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Gabriel Malheiros <gabriel.malheiros@serpro.gov.br>
 */

/**
 * Tasks Task Notifications
 *
 * @package     Tasks
 */
 class Tasks_Controller_TaskNotifications
 {
     const NOTIFICATION_LEVEL_NONE                      =  0;
     const NOTIFICATION_LEVEL_ASK                       = 10;
     const NOTIFICATION_LEVEL_AUTOMATIC                 = 20;

    /**
     * @var Tasks_Controller_EventNotifications
     */
    private static $_instance = NULL;

    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone()
    {
    }

    /**
     * the singleton pattern
     *
     * @return Tasks_Controller_TaskNotifications
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tasks_Controller_TaskNotifications();
        }

        return self::$_instance;
    }

    /**
     * constructor
     *
     */
    private function __construct()
    {

    }

    /**
     * send notifications
     *
     * @param Tasks_Model_Task       $_task
     * @param Tinebase_Model_FullAccount $_updater
     * @param Sting                      $_action
     * @param Tasks_Model_Task       $_oldTask
     * @return void
     */
    public function doSendNotifications($_task, $_updater, $_action, $_oldTask=NULL)
    {
        // we only send notifications to orginizer if  different of User task own.

        if ($_task->organizer) {
                $organizer = Tinebase_User::getInstance()->getFullUserById($_task->organizer);
            } else {
                // use creator as organizer
                $organizer = $_updater;
        }

       if (  $organizer->accountEmailAddress == $_updater->accountEmailAddress) {
            return;
        }
        switch ($_action) {
            case 'alarm':
                foreach($_event->attendee as $attender) {
                    if (Calendar_Model_Attender::isAlarmForAttendee($attender, $_alarm)) {
                      $this->sendNotificationToAttender($attender, $_event, $_updater, $_action, self::NOTIFICATION_LEVEL_NONE);
                    }
                }
                break;
            case 'created':
                   $this->sendNotificationToOrganizer($organizer, $_task, $_updater, $_action, self::NOTIFICATION_LEVEL_NONE);
                break;
            case 'deleted':
                  $this->sendNotificationToOrganizer($organizer, $_task, $_updater, $_action, self::NOTIFICATION_LEVEL_NONE);
                break;
            case 'changed':
                  $this->sendNotificationToOrganizer($organizer, $_task, $_updater, $_action, self::NOTIFICATION_LEVEL_NONE);
                break;
            default:
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " unknown action '$_action'");
                 return;
                 break;

        }


    }

    /**
     * send notification to a single attender
     *
     * @param Tinebase_Model_FullAccount $_organizer
     * @param Tasks_Model_Task           $_task
     * @param Tinebase_Model_FullAccount $_updater
     * @param Sting                      $_action
     * @param String                     $_notificationLevel
     * @param array                      $_updates
     * @param array                      $attachs
     * @return String                    Status
     */
    public function sendNotificationToOrganizer($_organizer, $_task, $_updater, $_action, $_notificationLevel, $_updates=NULL, $attachs = FALSE)
    {
        try {
            // get prefered language, timezone and notification level

            $prefUser = $_organizer->accountId;
            $locale = Tinebase_Translation::getLocale(Tinebase_Core::getPreference()->getValueForUser(Tinebase_Preference::LOCALE, $prefUser ? $prefUser : $_organizer->getId()));
            $timezone = Tinebase_Core::getPreference()->getValueForUser(Tinebase_Preference::TIMEZONE, $prefUser ? $prefUser : $_organizer->getId());
            $translate = Tinebase_Translation::getTranslation('Tasks', $locale);

            // check if user wants this notification
            $sendLevel          = $prefUser ? Tinebase_Core::getPreference('Tasks')->getValueForUser(Tasks_Preference::SEND_NOTIFICATION_TO_ORGANIZER, $prefUser) : 0;
            $sendOnOwnActions   = $prefUser ? Tinebase_Core::getPreference('Tasks')->getValueForUser(Tasks_Preference::NOTIFICATION_LEVEL, $prefUser) : 0;

            // NOTE: organizer gets mails unless she set notificationlevel to NONE
            if (!$sendOnOwnActions) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " The organaizer do not permit notification menssages");
                    return;
            }
            if (!$_organizer->hasGrant($_task->container_id, Tinebase_Model_Grants::GRANT_READ)) {
                    return;
            }

            // get date strings
            if($_task->start_time)
               $startDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_task->start_time, $timezone, $locale);
            if($_task->due)
               $dueDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_task->due, $timezone, $locale);
            if($_task->completed)
               $completedDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_task->completed, $timezone, $locale);
            switch ($_action) {
                case 'alarm':
                    $messageSubject = sprintf($translate->_('Alarm for task "%1$s"'), $_task->summary);
                    break;
                case 'created':
                    $messageSubject = sprintf($translate->_('Task  "%1$s" assigned to you'), $_task->summary);
                    break;
                case 'deleted':
                    $messageSubject = sprintf($translate->_('Task "%1$s"  has been canceled' ), $_task->summary);
                    break;
                case 'changed':
                    $messageSubject = sprintf($translate->_('Task "%1$s"  assigned to you, has been updated' ), $_task->summary);
                    break;
                default:
                    if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " unknown action '$_action'");
                    break;
            }

            $messageBody = $_task->getNotificationMessage();

            // NOTE: this is a contact as we only support users and groupmembers
            $calendarPart = null;
            $attachments = null;

            $contact = Addressbook_Controller_Contact::getInstance()->getContactByUserId($prefUser);
            $sender = $_action == 'alarm' ? $_organizer : $_updater;

            Tinebase_Notification::getInstance()->send($sender, array($contact), $messageSubject, $messageBody, $calendarPart, $attachments);
        } catch (Exception $e) {
            return;
        }
    }
 }
