<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 SERPRO (https://www.serpro.gov.br)
 * @author      Philipp Schuele <p.schuele@metaways.de>
 * @author      Bruno Vieira Costa <bruno.vieira-costa@serpro.gov.br>
 */



/**
 * Admin User ldap backend
 *
 * @package     Admin
 * @subpackage  User
 */
class Admin_Backend_MailUser extends Tinebase_EmailUser_Imap_Cyrus
{
    /**
     * delete user by id
     *
     * @param  Tinebase_Model_FullUser  $_user
     */
    public function logicDeleteUser(Tinebase_Model_FullUser $_user)
    {
        Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Delete Cyrus imap account of user ' . $this->_getUserLoginName($_user));

        $imap = $this->_getImapConnection();
        $mailboxString = $this->_getUserMailbox($this->_getUserLoginName($_user));
        $mailboxes = $imap->listMailbox('', str_replace('@', '*@', $mailboxString));
        $date = date("YmdHis");
        $acls = $imap->requestAndResponse("GETACL", array($mailboxString), $tag);
        for($i = 2; $i < count($acls[0]); $i = $i+2){
            foreach($mailboxes as $mailbox => $opts){
                $imap->setACL($mailbox, $acls[0][$i], '');
            }
        }

        $newMailboxString = 'user/'.str_replace('@', '_'.$date.'@', $this->_getUserLoginName($_user));
        Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " search for {$mailboxString} in " . print_r($mailboxes, true));
        // does mailbox exist at all?
        if (array_key_exists($mailboxString, $mailboxes)) {
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' must delete mailbox ');
            if ($imap->setACL($mailboxString, $this->_config['admin'], 'lrswipcda') === true) {
                $imap->rename($mailboxString,$newMailboxString);
                $imap->setACL($newMailboxString, $this->_config['admin'], '');
            }
        }
    }
}