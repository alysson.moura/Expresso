<?php
/**
 * Tine 2.0
 * @package     AppLauncher
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */

/**
 *
 * This class handles all Json requests for the AppLauncher application
 *
 * @package     AppLauncher
 * @subpackage  Frontend
 */
class AppLauncher_Frontend_Json extends Tinebase_Frontend_Json_Abstract
{
    /**
     * the controller
     *
     * @var AppLauncher_Controller_AppLauncherRecord
     */
    protected $_controller = NULL;
    
    /**
     * the models handled by this frontend
     * @var array
     */
    protected $_models = array('AppLauncherRecord');

    /**
     * the constructor
     *
     */
    public function __construct()
    {
        $this->_applicationName = 'AppLauncher';
        $this->_controller = AppLauncher_Controller_AppLauncherRecord::getInstance();
    }
    
    /**
     * Search for records matching given arguments
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     */
    public function searchAppLauncherRecords($filter, $paging)
    {
        return $this->_search($filter, $paging, $this->_controller, 'AppLauncher_Model_AppLauncherRecordFilter', TRUE);
    }
    
    /**
     * Return a single record
     *
     * @param   string $id
     * @return  array record data
     */
    public function getAppLauncherRecord($id)
    {
        return $this->_get($id, $this->_controller);
    }

    /**
     * creates/updates a record
     *
     * @param  array $recordData
     * @return array created/updated record
     */
    public function saveAppLauncherRecord($recordData)
    {
        return $this->_save($recordData, $this->_controller, 'AppLauncherRecord');
    }
    
    /**
     * deletes existing records
     *
     * @param  array  $ids 
     * @return string
     */
    public function deleteAppLauncherRecords($ids)
    {
        return $this->_delete($ids, $this->_controller);
    }    

    /**
     * Returns registry data
     * 
     * @return array
     */
    public function getRegistryData()
    {
        return array();
    }

}
