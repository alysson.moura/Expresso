<?php
/**
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @subpackage  Exception
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 *
 */

/**
 * AppLauncher exception
 * 
 * @package     AppLauncher
 * @subpackage  Exception
 */
class AppLauncher_Exception extends Exception
{
}
