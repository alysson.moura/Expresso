<?php
/**
 * Tine 2.0
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Prodemge (http://www.prodemge.gov.br)
 * @author      Victor Pinheiro <victor.pinheiro@prodemge.gov.br>
 *
 */

/**
 * Redirecionamento para o SSC
 *
 * @package     Prodemge
 * @subpackage  Tinebase
 */
class Prodemge_Tinebase_Server_Http implements Tinebase_Server_Http_Login_Plugin_Interface
{
    public static function init()
    {
        Tinebase_PluginManager::addGlobalPluginConfigItem('global_plugins_ssc_active', 'checkbox', 'SSC Active', FALSE);
        Tinebase_PluginManager::addGlobalPluginConfigItem('global_plugins_ssc_hostIDP', 'textfield', 'SSC HostIDP', '');
        Tinebase_PluginManager::addGlobalPluginConfigItem('global_plugins_ssc_referer', 'textfield', 'SSC Referer', '');
        Tinebase_PluginManager::addGlobalPluginConfigItem('global_plugins_ssc_appUrl', 'textfield', 'SSC AppUrl', '');
        Tinebase_PluginManager::addGlobalPluginConfigItem('global_plugins_ssc_urlLogout', 'textfield', 'SSC UrlLogout', '');
    }
    
    /**
     * @see Tinebase_Server_Http_Login_Plugin_Interface
     */
    public function doActions()
    {
        // verifica se a autenticação será via SSC
        if ( Tinebase_Core::getConfig()->global->plugins->ssc->active === true ) {

            // verifica se o login ja foi feito e o XML de resposta foi retornado da SSC
            if( isset($_REQUEST['SAMLResponse']) ) {
                // verificar o SAMLResponse se é do login ou do logout
                $samlResponse = base64_decode( $_REQUEST['SAMLResponse'] );
            }

            if(isset($samlResponse) && !(strpos($samlResponse, "LogoutResponse") )) {
                // ativa a sessão com os dados de usuário retornados do SSC
                $_REQUEST['method'] = 'Ssc.validaLoginSSC';

            } else {
                // dispara o login via SSC
                $ssc = new Prodemge_Tinebase_SscHelper();

                $hostIdpBase64 = $ssc->getHostIdpBase64();
                $refererBase64 = $ssc->getRefererBase64();
                $xmlLoginBase64 = $ssc->getXmlLoginBase64();
                $html = <<<HTML
<HTML>
    <BODY Onload="document.forms[0].submit()">
        <FORM METHOD="POST" ACTION="$hostIdpBase64">
            <INPUT TYPE="HIDDEN" NAME="Referer" VALUE="$refererBase64"/>
            <INPUT TYPE="HIDDEN" NAME="SAMLRequest" VALUE="$xmlLoginBase64"/>
        </FORM>
    </BODY>
</HTML>
HTML;
                echo $html;
                exit;
            }
        }
    }
}