<?php
/**
 * Association Shard key => Connection Config key
 *
 * @package Tinebase
 * @subpackage Shard
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * interface Association Shard key => Connection Config key
 *
 * @package Tinebase
 * @subpackage Shard
 */
interface Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Interface
{
   /**
    * holds the instance of the singleton
    *
    * @var Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Interface
    */
    public static function getInstance(array $_options, $_database);

   /**
    * get Connectionconfig key associated with given Shard key
    *
    * @param string $_key
    * @return string | FALSE
    */
    public function getAssociation($_key);

   /**
    * add Shard key X Connectionconfig key association
    *
    * @param integer $_key
    * @param string $_connectionConfigKey
    * @return boolean
    */
    public function addAssociation($_key, $_connectionConfigKey);

    /**
    * remove Shard key X Connectionconfig key association
    *
    * @param integer $_key
    * @return boolean
    */
    public function removeAssociation($_key);

    /**
    * clear all associations
    *
    */
    public function clearAllAssociations();
}