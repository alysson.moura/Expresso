See instructions at domains/default/shard.inc.php.dist

Look detailed documentation at http://comunidadeexpresso.serpro.gov.br

Examples of how to use shard tags in setup.xml of modules, can be found at Shard/examples folder.
This examples can be helpfull, if you intend to create a new separated backend to add more tables in Shard.