<?php
/**
 * class to hold ExampleRecord data
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>, Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */

class Webconference_Model_Room extends Tinebase_Record_Abstract
{  
    /**
     * supported status
     */
    const STATUS_ACTIVE    = 'A';
    const STATUS_EXPIRED   = 'E';
    
    /**
     * key in $_validators/$_properties array for the filed which 
     * represents the identifier
     * 
     * @var string
     */    
    protected $_identifier = 'id';    
    
    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Webconference';

    /**
     * list of zend validator
     * 
     * this validators get used when validating user generated content with Zend_Input_Filter
     *
     * @var array
     */
    protected $_validators = array(
        'id'                    => array(Zend_Filter_Input::ALLOW_EMPTY => true),
	'container_id'          => array('allowEmpty' => true,  'Int'  ),
    // @todo add more fields
    // modlog information
        'created_by'            => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'creation_time'         => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'last_modified_by'      => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'last_modified_time'    => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'is_deleted'            => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'deleted_time'          => array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'deleted_by'            => array(Zend_Filter_Input::ALLOW_EMPTY => true),

	//'room_user'		=> array('allowEmpty' => true        ),
	
	'title'			=> array(Zend_Filter_Input::ALLOW_EMPTY => true, Zend_Filter_Input::DEFAULT_VALUE => NULL),
        'room_name'             => array(Zend_Filter_Input::ALLOW_EMPTY => false),    
        'create_date'           => array(Zend_Filter_Input::ALLOW_EMPTY => true),
	'status'		=> array(Zend_Filter_Input::ALLOW_EMPTY => false, 'default' => 'A'),
	'wconf_config_id'	=> array(Zend_Filter_Input::ALLOW_EMPTY => true),
        'notes'                 => array('allowEmpty' => true         ), // originally comment handled by Tinebase_Notes
        'relations'             => array('allowEmpty' => true         ),

	'attendee'              => array('allowEmpty' => true         ), // RecordSet of Webconference_Model_Attender
	'organizer'             => array('allowEmpty' => false,        ),
        Tinebase_Model_Grants::GRANT_READ    => array('allowEmpty' => true),
        Tinebase_Model_Grants::GRANT_SYNC    => array('allowEmpty' => true),
        Tinebase_Model_Grants::GRANT_EXPORT  => array('allowEmpty' => true),
        Tinebase_Model_Grants::GRANT_EDIT    => array('allowEmpty' => true),
        Tinebase_Model_Grants::GRANT_DELETE  => array('allowEmpty' => true),
        Tinebase_Model_Grants::GRANT_PRIVATE => array('allowEmpty' => true),
    );

    /**
     * name of fields containing datetime or an array of datetime information
     *
     * @var array list of datetime fields
     */    
    protected $_datetimeFields = array(
        'creation_time',
        'last_modified_time',
        'deleted_time',
	'create_date',
	'call_date'
    );
    
    /**
     * overwrite constructor to add more filters
     *
     * @param mixed $_data
     * @param bool $_bypassFilters
     * @param mixed $_convertDates
     * @return void
     */
    public function __construct($_data = NULL, $_bypassFilters = false, $_convertDates = true)
    {
        // do something here if you like (add default empty values, ...)
	$this->_filters['organizer'] = new Zend_Filter_Empty(NULL);
        return parent::__construct($_data, $_bypassFilters, $_convertDates);
    }

    /**
     * fills a record from json data
     *
     * @param string $_data json encoded data
     * @return void
     */
    public function setFromJson($_data)
    {
        parent::setFromJson($_data);
    }

    /**
     * sets record related properties
     * 
     * @param string _name of property
     * @param mixed _value of property
     * @throws Tinebase_Exception_UnexpectedValue
     * @return void
     */
    public function __set($_name, $_value)
    {
        if ($_name == 'attendee' && is_array($_value)) {
            $_value = new Tinebase_Record_RecordSet('Webconference_Model_Attender', $_value);
        }
        parent::__set($_name, $_value);
    }
    
    /**
     * add current user to attendee if he's organizer
     * 
     * @param bool $ifOrganizer      only add current user if he's organizer
     * @param bool $ifNoOtherAttendee  only add current user if no other attendee are present
     */
    public function assertCurrentUserAsAttendee($ifOrganizer = TRUE, $ifNoOtherAttendee = FALSE)
    {
        if ($ifNoOtherAttendee && $this->attendee instanceof Tinebase_Record_RecordSet && $this->attendee->count() > 0) {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(
                    __METHOD__ . '::' . __LINE__ . " not adding current user as attendee as other attendee are present.");
            return;
        }
        
        $ownAttender = Webconference_Model_Attender::getOwnAttender($this->attendee);

        if (! $ownAttender) {
            if ($ifOrganizer && $this->organizer && $this->organizer != Tinebase_Core::getUser()->contact_id) {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(
                    __METHOD__ . '::' . __LINE__ . " not adding current user as attendee as current user is not organizer.");
            }
            else {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(
                    __METHOD__ . '::' . __LINE__ . " adding current user as attendee.");
                
                $newAttender = new Webconference_Model_Attender(array(
                    'user_id'   => Tinebase_Core::getUser()->contact_id,
                    'user_type' => Webconference_Model_Attender::USERTYPE_USER,
                ));
                
                if (! $this->attendee instanceof Tinebase_Record_RecordSet) {
                    $this->attendee = new Tinebase_Record_RecordSet('Webconference_Model_Attender');
                }
                $this->attendee->addRecord($newAttender);
            }
        }
    }
        /**
     * sets the record related properties from user generated input.
     * 
     * Input-filtering and validation by Zend_Filter_Input can enabled and disabled
     *
     * @param array $_data            the new data to set
     * @throws Tinebase_Exception_Record_Validation when content contains invalid or missing data
     */
    public function setFromArray(array $_data)
    {
        if (isset($_data['container_id']) && is_array($_data['container_id'])) {
            $_data['container_id'] = $_data['container_id']['id'];
        }
        
        if (isset($_data['organizer']) && is_array($_data['organizer'])) {
            $_data['organizer'] = $_data['organizer']['id'];
        }
        
        if (isset($_data['attendee']) && is_array($_data['attendee'])) {
            $_data['attendee'] = new Tinebase_Record_RecordSet('Webconference_Model_Attender', $_data['attendee'], $this->bypassFilters);
        }
        parent::setFromArray($_data);
    }   
    
    /**
     * checks event for given grant
     * 
     * @param  string $_grant
     * @return bool
     */
    public function hasGrant($_grant)
    {
        $hasGrant = array_key_exists($_grant, $this->_properties) && (bool)$this->{$_grant};
	$hasGrant &= (
	    // private grant
	    $this->{Tinebase_Model_Grants::GRANT_PRIVATE} ||
	    // I'm organizer
	    Tinebase_Core::getUser()->contact_id == ($this->organizer instanceof Addressbook_Model_Contact ? $this->organizer->getId() : $this->organizer) ||
	    // I'm attendee
	    Webconference_Model_Attender::getOwnAttender($this->attendee)
	);
        return $hasGrant;
    }
    
    /**
     * sets and returns the addressbook entry of the organizer
     * 
     * @return Addressbook_Model_Contact
     */
    public function resolveOrganizer()
    {
        if (! empty($this->organizer) && ! $this->organizer instanceof Addressbook_Model_Contact) {
            $contacts = Addressbook_Controller_Contact::getInstance()->getMultiple($this->organizer, TRUE);
            if (count($contacts)) {
                $this->organizer = $contacts->getFirstRecord();
            }
        }
        return $this->organizer;
    }
    
    /**
     * checks if given attendee is organizer of this event
     * 
     * @param Webconference_Model_Attendee $_attendee
     */
    public function isOrganizer($_attendee=NULL)
    {
        $organizerContactId = NULL;
        if ($_attendee && in_array($_attendee->user_type, array(Webconference_Model_Attender::USERTYPE_USER, Webconference_Model_Attender::USERTYPE_GROUPMEMBER))) {
            $organizerContactId = $_attendee->user_id instanceof Tinebase_Record_Abstract ? $_attendee->user_id->getId() : $_attendee->user_id;
        } else {
            $organizerContactId = Tinebase_Core::getUser()->contact_id;
        }
        return $organizerContactId == ($this->organizer instanceof Tinebase_Record_Abstract ? $this->organizer->getId() : $this->organizer);
    }
    
    public function getStatusString()
    {
        $statusConfig = Webconference_Config::getInstance()->roomStatus;
        $statusRecord = $statusConfig && $statusConfig->records instanceof Tinebase_Record_RecordSet ? $statusConfig->records->getById($this->status) : false;
        
        return $statusRecord? $statusRecord->value : $this->status;
    }
}