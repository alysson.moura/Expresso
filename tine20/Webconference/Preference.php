<?php
/**
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */


/**
 * backend for Webconference preferences
 *
 * @package     Webconference
 */
class Webconference_Preference extends Tinebase_Preference_Abstract
{  
    /**
     * default webconference all newly created/invited rooms are placed in
     */
    const DEFAULTCONFERENCE = 'webconference';
        
    /**
     * @var string application
     */
    protected $_application = 'Webconference';
        
    /**
     * get all possible application prefs
     *
     * @return  array   all application prefs
     */
    public function getAllApplicationPreferences()
    {
        $allPrefs = array(
            self::DEFAULTCONFERENCE,
        );
            
        return $allPrefs;
    }
    
    /**
     * get translated right descriptions
     * 
     * @return  array with translated descriptions for this applications preferences
     */
    public function getTranslatedPreferences()
    {
        $translate = Tinebase_Translation::getTranslation($this->_application);
        $prefDescriptions = array(
            self::DEFAULTCONFERENCE  => array(
                'label'         => $translate->_('Default Conference'),
                'description'   => $translate->_('The default conference for invitations and new rooms'),
            ),
        );
        return $prefDescriptions;
    }
    
    /**
     * get preference defaults if no default is found in the database
     *
     * @param string $_preferenceName
     * @param string|Tinebase_Model_User $_accountId
     * @param string $_accountType
     * @return Tinebase_Model_Preference
     */
    public function getApplicationPreferenceDefaults($_preferenceName, $_accountId = NULL, $_accountType = Tinebase_Acl_Rights::ACCOUNT_TYPE_USER)
    {
        $preference = $this->_getDefaultBasePreference($_preferenceName);
        switch($_preferenceName) {
            case self::DEFAULTCONFERENCE:
                $this->_getDefaultContainerPreferenceDefaults($preference, $_accountId);
                break;
            default:
                throw new Tinebase_Exception_NotFound('Default preference with name ' . $_preferenceName . ' not found.');
        }
        return $preference;
    }
}
