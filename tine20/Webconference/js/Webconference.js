/* 
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 */

Ext.ns('Tine.Webconference');

var WebconferenceOrigin = {
    MENU : 0,
    EMAIL : 1
}

Tine.Webconference.Application = Ext.extend(Tine.Tinebase.Application, {
    addButtonText: 'New Webconference',
    
    onExit: function(){
	this.logAccessLogoff();
	this.getMainScreen().getCenterPanel().pagingToolbar.doRefresh();
    },

    init: function() {
	Tine.Webconference.Application.superclass.init.apply(this.arguments);
	this.origin = WebconferenceOrigin.MENU;
         
        if (Tine.Expressomail) {
            Tine.Expressomail.MimeDisplayManager.register('text/webconference', Tine.Webconference.EmailDetailsPanel);
	}
      
	// binds link click event to open webconference module if reading email in Tine
	Ext.getBody().on('click', function(event, target){
            event.stopEvent();
            var url = Ext.get(target).select('a').elements[0].href;
            var params = Ext.urlDecode(url.split('?')[1]);
            var roomName = params.meetingID;
            Tine.Webconference.searchRoomName(roomName, function (response) {
                if (response.results.length > 0) {
                    Tine.Tinebase.appMgr.activate(Tine.Tinebase.appMgr.get('Webconference'));
                } else {
                    window.open(url);
                }
            });
        }, null, {
            delegate: 'span.tinebase-webconference-link'
        });
    },
    
    onJoinWebconferenceFromEmail: function(url, moderator, roomId, roomName){
	 console.log('URL: '+ url);
	 /*
	var loadMask = new Ext.LoadMask(Tine.Tinebase.appMgr.getActive().getMainScreen().getCenterPanel().getEl(), {
	    msg: String.format(_('Please wait'))
	});
	loadMask.show();
        
	var app = Tine.Tinebase.appMgr.get('Webconference');
	Tine.Webconference.isMeetingActive(roomId, url,  function(response) {
	    if(response.active){
		if(Tine.Tinebase.appMgr.get('Webconference').roomActive){
		    Ext.MessageBox.confirm('', 
			app.i18n._("You are already in a webconference. Accepting this invitation will make you leave the existing one.")+" "+
			app.i18n._('Proceed') + ' ?', function(btn) {
			    if(btn == 'yes') { 
				Tine.Tinebase.appMgr.get('Webconference').origin = WebconferenceOrigin.EMAIL;
				Tine.Tinebase.appMgr.get('Webconference').bbbUrl = url;
				Tine.Tinebase.appMgr.get('Webconference').roomName = roomName;
				Tine.Tinebase.appMgr.get('Webconference').roomId = roomId;
				Tine.Tinebase.appMgr.get('Webconference').moderator = moderator;
				Ext.get('webconference-iframe').dom.src = Tine.Tinebase.appMgr.get('Webconference').bbbUrl;
				Tine.Tinebase.appMgr.activate( Tine.Tinebase.appMgr.get('Webconference') );
				Tine.Tinebase.appMgr.get('Webconference').logAccessLogin();
			    }
			}, this);
		}
		else {
		    Tine.Tinebase.appMgr.get('Webconference').origin = WebconferenceOrigin.EMAIL;
		    Tine.Tinebase.appMgr.get('Webconference').bbbUrl = url;
		    Tine.Tinebase.appMgr.get('Webconference').roomName = roomName;
		    Tine.Tinebase.appMgr.get('Webconference').roomId = roomId;
		    Tine.Tinebase.appMgr.get('Webconference').moderator = moderator;
		    Tine.Tinebase.appMgr.get('Webconference').onMainScreen = false;
		    Tine.Tinebase.appMgr.get('Webconference').getMainScreen().updateContentPanel();
		    Tine.Tinebase.appMgr.get('Webconference').getMainScreen().show();
		    Tine.Tinebase.appMgr.get('Webconference').collapseWestPanel();
		    Tine.Tinebase.appMgr.activate( Tine.Tinebase.appMgr.get('Webconference') ); 
		    Tine.Tinebase.appMgr.get('Webconference').logAccessLogin();
		}
	    }
	    else{
		Ext.MessageBox.show({
		    title: Tine.Tinebase.appMgr.get('Webconference').i18n._('Webconference'), 
		    msg: response.message,
		    buttons: Ext.Msg.OK,
		    icon: Ext.MessageBox.INFO
		});
	    }
	    loadMask.hide();
	});
	*/
    },
    
    logAccessLogin: function(){   
	Ext.Ajax.request({
	    params: {
		method: 'Webconference.logAccessLogon',
		roomId: Tine.Tinebase.appMgr.get('Webconference').roomId
	    },
	    scope: this,
	    success: function(_result, _request){
		var result = Ext.util.JSON.decode(_result.responseText);
		Tine.Tinebase.appMgr.get('Webconference').idAccessLog = result;
	    }
	});
    },
    
    logAccessLogoff: function(){
	Ext.Ajax.request({
	    params: {
		method: 'Webconference.regLogoff',
		idAccess: Tine.Tinebase.appMgr.get('Webconference').idAccessLog
	    },
	    scope: this,
	    success: function(_result, _request){
		
	    }
	});
    }
});

/**
 * generic exception handler for filemanager
 * 
 * @param {Tine.Exception} exception
 */
Tine.Webconference.handleRequestException = function(exception, request) {
    var app = Tine.Tinebase.appMgr.get('Webconference');
    if (exception.code === 404) {
	Ext.Msg.show({
	   title:   app.i18n._('Save Room Error'),
	   msg:     app.i18n._('Could not save Room.') + (exception.message ? ' (' + exception.message + ')' : ''),
	   icon:    Ext.MessageBox.ERROR,
	   buttons: Ext.Msg.OK
	});
	app.getMainScreen().getCenterPanel().pagingToolbar.doRefresh();
    } 
    else {
	Tine.Tinebase.ExceptionHandler.handleRequestException(exception);
    }
};

// default mainscreen
Tine.Webconference.MainScreen = Ext.extend(Tine.widgets.MainScreen, {
    activeContentType: 'Room'
});

Tine.Webconference.RoomTreePanel = function(config) {
    this.id = 'RoomTreePanel';
    this.recordClass = Tine.Webconference.Model.Room;
    
    this.filterMode = 'filterToolbar'; 
    Tine.Webconference.RoomTreePanel.superclass.constructor.call(this);
};

Tine.Webconference.RoomFilterPanel = function(config) {
    Ext.apply(this, config);
    Tine.Webconference.RoomFilterPanel.superclass.constructor.call(this);
};

Ext.extend(Tine.Webconference.RoomFilterPanel, Tine.widgets.persistentfilter.PickerPanel, {
    filter: [{field: 'model', operator: 'equals', value: 'Webconference_Model_RoomFilter'}]
});
/*
Tine.Webconference.TreePanel = Ext.extend(Tine.widgets.container.TreePanel, {
            onBeforeClick: function(node, e) {
        if (node.attributes.path.match(/^\/$|^\/personal$/)) {
	    this.onClick(node, e);
            return false;
        }
}
});
*/
/*

Tine.Webconference.ListTreePanel = function(config) {
    Ext.apply(this, config);
    
    this.id = 'Addressbook_List_Tree';
    this.filterMode = 'filterToolbar';
    this.recordClass = Tine.Webconference.Model.Room;
    Tine.Webconference.ListTreePanel.superclass.constructor.call(this);

};
Ext.extend(Tine.Webconference.ListTreePanel , Tine.widgets.container.TreePanel, {
        onBeforeClick: function(node, e) {
        if (node.attributes.path.match(/^\/$|^\/personal$/)) {

		this.onClick(node, e);
            return false;
        }
    }
});
*/

Tine.Webconference.RoomTreePanel = Ext.extend(Tine.widgets.container.TreePanel, {

    recordClass: Tine.Webconference.Model.Room,
    backendModel: Tine.Webconference.Model.Room, 
    ddGroup: 'room-event',
    filterMode: 'filterToolbar',
    useProperties: true,
    
    initComponent: function() {
        this.filterPlugin = new Tine.widgets.tree.FilterPlugin({
            treePanel: this,
            getGridPanel: function() {
                return Tine.Tinebase.appMgr.get('Webconference').getMainScreen().getCenterPanel();
            }
        });
        
        this.on('beforeclick', this.onBeforeClick, this);
        this.on('containercolorset', function() {
            Tine.Tinebase.appMgr.get('Webconference').getMainScreen().getCenterPanel().refresh(true);
        });
        
        this.supr().initComponent.call(this);
    },
    
    /**
     * dissalow loading of all and otherUsers node
     * 
     * @param {Ext.tree.TreeNode} node
     * @param {Ext.EventObject} e
     * @return {Boolean}
     */
    onBeforeClick: function(node, e) {
        if (node.attributes.path.match(/^\/$|^\/personal$/)) {
            this.onClick(node, e);
            return false;
        }
    },
    
    /**
     * adopt attr
     * 
     * @param {Object} attr
     */
    onBeforeCreateNode: function(attr) {
        this.supr().onBeforeCreateNode.apply(this, arguments);
        if (attr.container) {
            attr.container.capabilites_private = true;
        }
    },
    
    /**
     * called when events are droped on a webconference node
     * 
     * NOTE: atm. event panels only allow d&d for single events
     * 
     * @private
     * @param  {Ext.Event} dropEvent
     * @return {Boolean}
     */
    onBeforeNodeDrop: function(dropEvent) {
        var containerData = dropEvent.target.attributes,
            selection = dropEvent.data.selections,
            mainScreenPanel = Tine.Tinebase.appMgr.get('Webconference').getMainScreen().getCenterPanel(),
            abort = false;

        // @todo move this to dragOver
        if (! containerData.account_grants.addGrant) {
            abort = true;
        }
        
        Ext.each(selection, function(event) {
            if (Tine.Tinebase.container.pathIsMyPersonalContainer(event.get('container_id').path)) {
                // origin container will only be moved for personal events with their origin in
                // a personal container of the current user
                event.set('container_id', containerData.id);
                mainScreenPanel.onUpdateEvent(event);

                dropEvent.cancel = false;
                dropEvent.dropStatus = true;
                
            } else {
                // @todo move displaycal if curruser is attender
                abort = true;
            }
        }, this);
        
        if (abort) {
            return false;
        }
    }
});
